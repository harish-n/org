const fs = require("fs");

let sponsorsReportFilePath = process.argv[2]

function getDate(dateString) {
    let date = new Date(dateString);
    let options = { month: "long" };
    let month = new Intl.DateTimeFormat("en-US", options).format(date);
    return `${month} ${date.getDate()}, ${date.getFullYear()}`;
}

function getTotalAmount(transactions) {
    let totalAmount = 0;
    transactions.map((transaction) => {
        let amount = Number(transaction.processed_amount.replace("$", ""));
        totalAmount = totalAmount + amount;
    });
    return totalAmount;
}

fs.readFile(sponsorsReportFilePath, (err, data) => {
    if (err) throw err;
    const reportSponsors = JSON.parse(data);
    const formattedSponsors = reportSponsors.map(reportSponsor => {
        let transactions = reportSponsor.transactions;
        let monthlyAmount = reportSponsor.transactions[0].tier_monthly_amount
        return {
            username: reportSponsor.sponsor_handle,
            monthlyAmount: Number(monthlyAmount.replace("$", "")),
            totalAmount: getTotalAmount(transactions),
            startDate: getDate(reportSponsor.sponsorship_started_on),
            endDate: getDate(transactions[0].transaction_date),
        }
    });
    fs.writeFile('scripts/parse-sponsors-report/sponsors.json', JSON.stringify({ sponsors: formattedSponsors }), function (err) {
        if (err) throw err;
        console.log('Sponsors.json File Generated');
    }
    );

});
