
# Sponsor Data Parser

This script formats the Github Sponsors report into one that fits the sponsors.json file used by the landing website

## Script Execution

 ```bash
yarn parse-sponsors-report  '<sponsorsReportFilePath>' 
```
