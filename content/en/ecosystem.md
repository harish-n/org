---
title: Ecosystem 🌐
description: Learn more about the online ecosystem we're building to help democratize education through open source software.
category: Info
position: 2
---

## Landing Website

Our little corner of the Internet.

<cta-button text="Visit Landing" link="https://grey.software"></cta-button>

## Org Website

A website that provides transparent access to information about Grey Software for our collaborators and the general public.

<alert type="success">You're already on the org website</alert>

## Onboarding Website

A practical intro to open source software development for new team members or curious explorers.

<cta-button text="Start Onboarding" link="https://onboarding.grey.software"></cta-button>

## Learning Website

A website where we create useful learning material for the open-source software ecosystem.

<cta-button text="Start Learning" link="https://learn.grey.software"></cta-button>

## Resources Website

A website where we curate useful resources for the open-source software ecosystem.

<cta-button text="View Resources" link="https://resources.grey.software"></cta-button>

## Glossary Website

A website where someone new to technical terms can explore their meanings and learn more about them.

<cta-button text="Visit Glossary" link="https://glossary.grey.software"></cta-button>

## Focused Browsing

A browser extension we published to the Chrome and Firefox marketplaces that allows users to focus by hiding distracting feeds on popular websites.

<extension-links chrome="https://chrome.google.com/webstore/detail/ocbkghddheomencfpdiblibbjhjcojna" firefox="https://addons.mozilla.org/en-US/firefox/addon/focused-browsing/"></extension-links>

## Material Math

A project concept for a web app that allows you to practice mental math with a fun, beautiful interface.

<cta-button text="Demo" link="https://material-math.grey.software"></cta-button>

## Toonin

A project concept for a web app that allows you to tune in to your friends and family in realtime using peer-to-peer sharing.

<cta-button text="Demo" link="https://toonin.grey.software"></cta-button>
<br></br>
