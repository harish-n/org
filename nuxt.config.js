import theme from 'grey-docs'

export default theme({
    components: true,
    content: {
        liveEdit: false
    },
    docs: {
        primaryColor: '#64748b'
    },
    head: {
        title: 'Grey Software Org | Democratizing Education through Open Software!',
        meta: [
            {
                name: 'og:title',
                content: 'Grey Software Org | Democratizing Education through Open Software!'
            },
            {
                name: 'og:description',
                content: 'Grey Software is a not-for-profit organization on a mission to democratize software education.'
            },
            {
                name: 'og:image',
                content: '/preview.png'
            },
            {
                name: 'twitter:card',
                content: 'summary_large_image'
            },
            {
                name: 'twitter:title',
                content: 'Grey Software Org | Democratizing Education through Open Software!'
            },
            {
                name: 'twitter:description',
                content: 'Grey Software is a not-for-profit organization on a mission to democratize software education.'
            },
            {
                name: 'twitter:image',
                content: '/preview.png'
            },
        ],
        script: [
            {
                src: 'https://plausible.io/js/plausible.js',
                async: true,
                defer: true,
                'data-domain': 'org.grey.software',
            },
            {
                async: true,
                src: 'https://plausible.io/js/embed.host.js',
            }
        ]
    },
})

